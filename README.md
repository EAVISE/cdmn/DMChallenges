# Decision Management Community: Collection of Challenges

This repository contains a collection of all the DMC challenges + solutions used in our publication on cDMN, all bundled together for posterity's sake.
We claim no ownership of the material in this repository -- all credits go to the original authors.

For a full list of all descriptions, check out [the challenges directly on the DMC website](https://dmcommunity.org/challenge/).
